#include "torque_qp/controller.h"

namespace QPController
{
bool Torque::init(std::string robot_description, std::vector<std::string> joint_names) 
{
    //--------------------------------------
    // LOAD ROBOT
    //--------------------------------------
    if (!load_robot_model(robot_description,joint_names))
        return false;
    if (!control_frame_set)
        controlled_frame = model.frames[model.nframes-1].name;
    setRegularizationWeight(1e-5);
    setControlPeriod(0.001);
    k_reg = Eigen::MatrixXd::Ones(7,1) * 1.0; 
    dtorque_max = Eigen::MatrixXd::Ones(7,1) * 1000.0; 

    //--------------------------------------
    // QPOASES
    //--------------------------------------
    number_of_variables = model.nv;
    number_of_constraints = model.nv;
    
    qp = qp_solver.configureQP(number_of_variables, number_of_constraints);
    return true;
}

Eigen::VectorXd Torque::update(Eigen::VectorXd q, Eigen::VectorXd qdot, Eigen::VectorXd tau_J_d, Eigen::Matrix<double,6,1> xdd_star)
{
    // Update the model
    // First calls the forwardKinematics on the model, then computes the placement of each frame.
    pinocchio::forwardKinematics(model,data,q,qdot,0.0*qdot);
    pinocchio::updateFramePlacements(model,data);
    pinocchio::computeJointJacobians(model,data,q);
    pinocchio::getFrameJacobian(model, data, model.getFrameId(controlled_frame), pinocchio::ReferenceFrame::LOCAL, J);
    pinocchio::computeMinverse(model,data,q);
    data.Minv.triangularView<Eigen::StrictlyLower>() = data.Minv.transpose().triangularView<Eigen::StrictlyLower>();
    pinocchio::computeGeneralizedGravity(model,data,q);
    pinocchio::computeCoriolisMatrix(model,data,q,qdot);
    pinocchio::computeJointJacobiansTimeVariation(model,data,q,qdot);
    Jdot_qdot = pinocchio::getFrameClassicalAcceleration(model, data, model.getFrameId(controlled_frame),pinocchio::ReferenceFrame::LOCAL).toVector();

    // Formulate QP problem such that
    // joint_torque_out = argmin 1/2 tau^T H tau + tau^T g_
    //                       s.t     lbA < A tau < ubA
    //                                   lb < tau < ub 
    nonLinearTerms = data.Minv * (data.C* qdot + data.g);
    
    qp.hessian  = 2.0 * regularisation_weight *  data.Minv;
    // qp.gradient = - 2.0 * regularisation_weight * data.Minv *
    //               (data.g - (k_reg.asDiagonal() * qdot + k_qmid.asDiagonal() * (q-qmid)));
    qp.gradient = - 2.0 * regularisation_weight * data.Minv * (data.g - k_reg.asDiagonal() * qdot );              

    a.noalias() = J * data.Minv;
    b.noalias() =  Jdot_qdot - J * nonLinearTerms - xdd_star;

        qp.hessian  +=  2.0 * a.transpose() *  a;
        qp.gradient +=  2.0 * a.transpose() * b;    

    qp.ub = ( model.effortLimit);//.cwiseMin( dtorque_max * control_period + tau_J_d);
    qp.lb = (-model.effortLimit);//.cwiseMax(-dtorque_max * control_period + tau_J_d);


    double horizon_dt = 15 * control_period;
    qp.a_constraints.block(0, 0, model.nv, model.nv) = data.Minv;
    
    qp.lb_a.block(0, 0, model.nv, 1) =
        ((-model.velocityLimit - qdot) / horizon_dt + nonLinearTerms)
            .cwiseMax(2 * (model.lowerPositionLimit - q - qdot * horizon_dt) / (horizon_dt * horizon_dt) + nonLinearTerms);

    qp.ub_a.block(0, 0, model.nv, 1) =
        ((model.velocityLimit - qdot) / horizon_dt + nonLinearTerms)
            .cwiseMin(2 * (model.upperPositionLimit - q - qdot * horizon_dt) / (horizon_dt * horizon_dt) + nonLinearTerms);

    auto solution =  qp_solver.SolveQP(qp);

    return solution - data.g;
}


bool Torque::load_robot_model(std::string robot_description, std::vector<std::string> joint_names)
{
    // Load the urdf model
    pinocchio::urdf::buildModelFromXML(robot_description,model,false);
    if (!joint_names.empty())
    {
        std::vector<pinocchio::JointIndex> list_of_joints_to_keep_unlocked_by_id;
        for(std::vector<std::string>::const_iterator it = joint_names.begin();
            it != joint_names.end(); ++it)
        {
            const std::string & joint_name = *it;
            if(model.existJointName(joint_name))
            list_of_joints_to_keep_unlocked_by_id.push_back(model.getJointId(joint_name));
            else
            std::cout << "joint: " << joint_name << " does not belong to the model.";
        }
        
        // Transform the list into a list of joints to lock
        std::vector<pinocchio::JointIndex> list_of_joints_to_lock_by_id;
        for(pinocchio::JointIndex joint_id = 1; joint_id < model.joints.size(); ++joint_id)
        {
            const std::string joint_name = model.names[joint_id];
            if(is_in_vector(joint_names,joint_name))
            continue;
            else
            {
            list_of_joints_to_lock_by_id.push_back(joint_id);
            }
        }
        
        // Build the reduced model from the list of lock joints
        Eigen::VectorXd q_rand = randomConfiguration(model);
        model = pinocchio::buildReducedModel(model,list_of_joints_to_lock_by_id,q_rand);
    }
    data = pinocchio::Data(model);

    // Resize and initialise varialbes 
    J.resize(6,model.nv);
    J.setZero();
    nonLinearTerms.resize(model.nv);
    a.resize(6,model.nv);

    return true;
}

pinocchio::Model Torque::getRobotModel()
{
    return model;
}

void Torque::setControlledFrame(std::string controlled_frame)
{
    this->controlled_frame = controlled_frame; 
    control_frame_set = true;
}

std::string Torque::getControlledFrame()
{
    return controlled_frame;
}

void Torque::setRegularizationWeight(double regularisation_weight)
{
    this->regularisation_weight = regularisation_weight; 
}

void Torque::setDTorqueMax(Eigen::VectorXd dtorque_max)
{
    this->dtorque_max = dtorque_max; 
}

void Torque::setRegularizationGains(Eigen::VectorXd regularisation_gains)
{
    this->k_reg = regularisation_gains; 
}


void Torque::setControlPeriod(double control_period)
{
    this->control_period = control_period; 
}


} // namespace QPController
